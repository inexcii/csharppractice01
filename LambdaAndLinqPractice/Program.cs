﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LambdaAndLinqPractice
{
    class Program
    {
        private static bool FilterNumber(int number)
        {
            return number % 7 == 2;
        }

        static void Main()
        {
            var originalData = Enumerable.Range(1, 10000);

            // Query syntax.

            var filteredNumbers =
                from number in originalData
                where number % 7 == 2
                select Convert.ToString(number, 16);

            var resultQuery =
                from text in filteredNumbers
                where text.Contains("F") || text.Contains("f")
                select Convert.ToInt32(text, 16);


            // Method syntax.

            //var resultQuery = originalData
            //    .Where(number => number % 7 == 2)
            //    .Select(n => Convert.ToString(n, 16))
            //    .Where(text => text.Contains("F") || text.Contains("f"))
            //    .Select(t => Convert.ToInt32(t, 16));


            // Output:

            foreach (int num in resultQuery)
            {
                System.Console.Write(num + " ");
            }

            System.Console.ReadKey();
        }
    }
}
